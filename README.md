# GeoFence

Sample application that uses maxmind's geoip2 database to validate if an IP belongs within the provided whitelist of countries.

The service consists of a dockerized fastAPI implementation running on a standard
containerized Uvicorn/Gunicorn server.

The service provides one endpoint to access (HTTP GET only) the location resource
using the IP address as the resource ID and an optional countries parameter (see
examples below):
```http://127.0.0.1/location/{ip_address}```

```json
{
  "whitelist_match": "<boolean value>",
  "ip": "<IP address>",
  "iso_country_code": "<IP location country>"
}
```

If invoked without the countries parameter, the service returns the matched
geography with a whitelist_match value of False. If, however, a whitelist is
provided, the service will return a 404 if the IP is found in a different
geography.

---
### Build and run container
```docker build -t geofence . ; docker run --name geocontainer -p 80:80 geofence```

To stop and cleanup:
```docker stop geocontainer; docker rm geocontainer```

---
### API Swagger Docs
```http://127.0.0.1/docs#/```
```http://127.0.0.1/redoc#/```

---
### Examples
```http://127.0.0.1/location/176.31.84.249?countries=US,FR```

```http://127.0.0.1/location/176.31.84.249```

```http://127.0.0.1/location/176.31.84.249?countries=US```

```http://127.0.0.1/location/176.31.84.249?countries=FR```

---
### Updating the database
This sample application does not currently implement a mechanism to
update geopIP DB but this should be fairly straightforward to implement
by leveraging a shared docker volume (or a k8s persistent volume) in
combination with the maxmind provided [docker image](https://hub.docker.com/r/maxmindinc/geoipupdate).
This could be something that runs on a periodic schedule (say nightly) via the
GEOIPUPDATE_FREQUENCY parameter to the update container.

---
### Possible service extensions
There are a couple of extensions to the service capability that
feel like they'd be useful depending on the use case:
* more granular location lookups - city, ASN etc. and correspondingly,
  more fine-grained filters.
* Ability to blacklist certain IPs, even if they fall within the whitelist.
* More detailed Location model to provide data such as accuracy, timezone,
  currency etc.
* Support for multiple content types and interfaces.
