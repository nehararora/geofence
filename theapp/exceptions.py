"""
exceptions.py: Application specific exception definitions.
"""


class LocationError(Exception):
    """
    Base location service exception class.
    """
    def __init__(self, code: int, message: str):
        """
        Exception initializer.
        :param code: error code
        :param msg: Message string
        """
        super().__init__(message)
        self.code = code
        self.message = message
