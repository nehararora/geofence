"""
location.py: Lightweight interface module to the maxmind geoip database.
"""
import logging

import geoip2.database
import geoip2.errors

from exceptions import LocationError


# We'll create the reader once (will get instantiated at module load time)
# since the maxmind docs state this is an expensive op and the obj is reusable.
reader = geoip2.database.Reader("GeoLite2-Country.mmdb")


def lookup_ip(ip_address: str, countries: [str]) -> dict:
    """
    Method to lookup the location of a given IP.

    :param ip_address: IP address being looked up
    :param countries: List of ios-3166 formatted country codes
    :return:
    """
    # TODO: error handling
    try:
        response = reader.country(ip_address)
    except geoip2.errors.AddressNotFoundError:
        # We will handle this in the main module as an invalid IP.
        logging.error("Address not found in DB.")
        raise LocationError(400, "Address not found in DB.")

    # If we've got a whitelist, we check against that first, if not we'll return
    # location information about the IP as-is. This will let us keep a "RESTy"
    # interface where the url refers to the location resource.
    whitelist_match: bool = False
    if not countries:
        logging.debug(f"IP matched country code {response.country.iso_code}")
        whitelist_match = False
        pass
    else:
        for country in countries:
            # check if the IP is in one of the whitelisted countries
            if response.country.iso_code == country:
                logging.debug(f"IP Found in {country}")
                # we can break out after the first match since IP can only match
                # one location
                whitelist_match = True
                break
        else:
            # This means we've got a whitelist and didn't match any country
            raise LocationError(code=404, message="IP not in whitelist")

    # TODO: we've got the matched country in response, so we parse out the
    #  fields we want and return our response
    resp: dict = {
        "whitelist_match": whitelist_match,
        "ip": ip_address,
        "iso_country_code": response.country.iso_code,
    }
    return resp
