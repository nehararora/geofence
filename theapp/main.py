
import logging
import ipaddress

from typing import Optional
from iso3166 import countries as iso_countries
from fastapi import FastAPI, HTTPException

import location
from models import Location, Message

logging.basicConfig(format="[%(asctime)s] %(message)s", level=logging.DEBUG)

app = FastAPI()
logging.debug("Application object created")


@app.get(
    "/location/{ip}",
    response_model=Location,
    responses={
        404: {"model": Message},
        400: {"model": Message},
    }
)
def get_location(ip: str, countries: Optional[str] = None):
    """
    Validate given IP address against provided country whitelist. If the
    countries parameter is not provided, the location object is returned
    but with a whitelist_match value of False.

    Whitelist (if specified) must be provided as iso3166 (two character) formatted comma separated values.

    An HTTP error code of 404 indicates that the IP did not match any of the
    provide countries in the whitelist.

    Example: http://127.0.0.1/location/176.31.84.249?countries=US,FR

    Example: http://127.0.0.1/location/176.31.84.249

    Example: http://127.0.0.1/location/176.31.84.249?countries=US
    """

    logging.debug(f"countries are: {countries}")
    # Convert countries from comma separated string to list
    cntrys: [str] = countries.split(",") if countries is not None else []

    # validate parameters
    logging.debug("Validating request parameters")
    validate_params(ip, cntrys)

    try:
        response = location.lookup_ip(ip_address=ip, countries=cntrys)
        logging.debug(f"geoip2 DB response: {response}")
        return response
    except location.LocationError as lerr:
        logging.error(lerr)
        raise HTTPException(status_code=lerr.code, detail=lerr.message)


def validate_params(ip: str, countries: [str] = None):
    """
    Ensure IP address and country code are valid
    :param ip: presumably the IP address
    :param countries: presumably a list of ISO-3166 format country codes.
    :return:
    """
    # validate IP address
    try:
        ipaddress.ip_address(ip)
    except ValueError:
        logging.error("Invalid IP address")
        # raise exception if not good
        raise HTTPException(status_code=400, detail="Bad IP address.")
    # TODO: validate iso country codes
    for country in countries:
        try:
            iso_countries.get(country)
        except KeyError:
            msg: str = f"Invalid country code '{country}' in whitelist"
            raise HTTPException(status_code=400, detail=msg)
