"""
models.py: Application model classes.
"""

from pydantic import BaseModel


class Location(BaseModel):
    """
    Location Response model.
    """
    whitelist_match: bool  # True if IP was found in whitelist
    ip: str  # IP being searched
    iso_country_code: str  # country matching the IP


class Message(BaseModel):
    detail: str
